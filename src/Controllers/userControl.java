package Controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.silent.data.UserPostgres;

import com.silent.beans.User;
import com.silent.service.UserService.java;

public class userControl {
	ObjectMapper om = new ObjectMapper();
	UserPostgres up = new UserPostgres();

	UserService userserv = new UserService();
	
	public String redirectUser(HttpServletRequest req) {
		User u = (User) req.getSession().getAttribute("user");
		if (u.getRole().getRole().equals("employee")) {
			return "html/new.html";
		} else {
			return "html/all.html";
		}
	}
	
public String login(HttpServletRequest req) {
		
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		User u = userserv.validateUser(username, password);
		if(u == null ) {
			return "login.page";
		} else {
			req.getSession().setAttribute("user", u);
			return redirectUser(req);
			
			
		}
	
}
public String logout(HttpServletRequest req) {
	// TODO Auto-generated method stub
	return null;
}

public String register(HttpServletRequest req) throws JsonParseException, JsonMappingException, IOException {

	User user = om.readValue(req.getInputStream(), User.class);
	user = us.createUser(user);
	return "login.page";
}
}