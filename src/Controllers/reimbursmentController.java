package Controllers;

import com.silent.services.ReimbursmentService;

public class reimbursmentController {
	private ObjectMapper om = new ObjectMapper();
	private ReimbursmentService rs;
	
	
	public ReimbursmentDataController() {
		this(new ReimbursmentService());
	}

	public void ReimbursementDataController(ReimbursmentService rs) {
		super();
		this.rs = rs;
	}

	public void sendAllDataByUser(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		User user = (User) req.getSession().getAttribute("user");
		Set<Reimbursment> reimburs = rs.getByUser(user);
		res.getWriter().write(om.writeValueAsString(reimbs));
	}

	public void sendAllDataByStatus(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
				
		String status =  req.getParameter("statusName");
		Set<Reimbursment> reimburs = rs.getAllByStatus(status);
		res.getWriter().write(om.writeValueAsString(reimbs));
		
	}

	public void sendAllData(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		Set<Reimbursment> reimburs = rs.getAll();
		res.getWriter().write(om.writeValueAsString(reimburs));
		
		
	}

	public void update(HttpServletRequest req, HttpServletResponse res) {
		// TODO Auto-generated method stub
		
	}

}
