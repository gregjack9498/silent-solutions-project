package com.silent.services;

import java.util.Set;
import com.silent.services.*;

import com.silent.beans.Reimbursment;
import com.silent.beans.ReimbursmentStatus;
import com.silent.beans.User;
import com.silent.data.ReimbursmentDAO;


public class ReimbursmentService {

//	private ReimbursmentDAO reimDAO;
//	
//	public ReimbursmentService(ReimbursmentDAO rd) {
//		
//		reimDAO =rd;
//		
//	}
//	public int createReimbursment(User u, Reimbursment r) {
//		return reimDAO.createReimbursment(u, r);
//	}
//	public Set<Reimbursment> findReimbursmentbyUser(User u){
//		return reimDAO.findReimbursmentByName(name);
//	}
	
	

private ReimbursmentPostgres rd;

public ReimbursmentService() {
	this(new ReimbursmentPostgres());
}
public ReimbursmentService(ReimbursmentPostgres rd) {
	super();
	this.rd = rd;
}
public Set<Reimbursment> getAll() {
	return rd.getAllReimbursments();
}
public Set<Reimbursment> getByUser(User user) {
	return rd.getReimbursmentByUser(user);
}
public Set<Reimbursment> getAllByStatus(String statusName) {
	ReimbursmentStatus status = rd.getStatusByName(statusName);
	return rd.getReimbursmentsByStatus(status);
}
}