package com.silent.services;

import com.silent.beans.Role;
import com.silent.beans.User;
import com.silent.data.UserDAO;
import com.silent.data.UserPostgres;

public class UserService {

	private UserDAO ud;
	
	public UserService() {
		this.ud= new UserPostgres();
	}
	public User validateUser(String username, String password) {
		return ud.confirmCredentials(username,password);
	}
	public User createUser(User u) {
		Role role = ud.findRoleById("employee");
		u.setRole(role);
		return ud.createUser(u);
	}
	
}


