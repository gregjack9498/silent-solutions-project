package com.silent.data;
import java.util.Set;
import com.silent.beans.*;



public interface ReimbursmentDAO {

	public int createReimbursment( User u, Reimbursment r);
	public Set<Reimbursment> findReimbursmentById(User u);
	public ReimbursmentStatus findReimbursmentByName(String name);//Reimbursement names 
	public ReimbursmentStatus findReimbursmentById(int id);//Reimbursements 
	public Set<Reimbursment> getAllApprovedReimbursments();//approved Reimbursements
	public Set<Reimbursment> getAllDeniedReimbursments();//denied Reimbursements
	public boolean updateUserReimbursment(ReimbursmentStatus reimbursmentstatus, int id);
//	public ReimbursmentStatus getReimbursmentByName(String name);
	
	
}
