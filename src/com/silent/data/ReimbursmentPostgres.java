package com.silent.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import com.silent.beans.Reimbursment;
import com.silent.beans.ReimbursmentStatus;
import com.silent.beans.ReimbursmentType;
import com.silent.beans.User;

import utilities.ConnectionUtil;

//import utilities.ConnectionUtil;
//
//public class ReimbursmentPostgres implements ReimbursmentDAO {
//
//	private ConnectionUtil cu = ConnectionUtil.getConnection();
//	
//	@Override
//	public int createReimbursment(User u, Reimbursment r) {
//		int id =0;
//	}
//
//	public Set<Reimbursment> getAllReimbursments() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	public Set<Reimbursment> getReimbursmentByUser(User user) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	public ReimbursmentStatus getStatusByName(String statusName) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	public Set<Reimbursment> getReimbursmentsByStatus(ReimbursmentStatus status) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//	
}
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.HashSet;
//import java.util.Set;
//
//import dev.iafrate.model.Reimbursement;
//import dev.iafrate.model.ReimbursementStatus;
//import dev.iafrate.model.ReimbursementType;
//import dev.iafrate.model.User;
//import dev.iafrate.utils.ConnectionUtil;

public class ReimbursementPostgres implements ReimbursementDAO {
	
	ConnectionUtil cu;
	UserPostgres userp = new UserPostgres();
	
	
	
	public ReimbursementPostgres() {
		super();
		this.cu = ConnectionUtil.getConnectionUtil();
	}

	@Override
	public Reimbursment createReimbursement(Reimbursment r) {
		String sql = "insert into ers_reimbursement (reimb_id , reimb_amount , reimb_submitted , reimb_author , reimb_status_id , reimb_type_id , reimb_description) values"
				+ " (default, ?, current_timestamp, ?, ?, ?, ?)";
		String[] keys = {"reimb_id"};
		
		try(Connection conn = cu.getConnection()){
			PreparedStatement pstate = conn.prepareStatement(sql, keys);
			pstate.setDouble(1, r.getSubmittedAmount());
			pstate.setInt(2, r.getAuthor().getUserId());
			pstate.setInt(3, r.getStatus().getStatusId());
			pstate.setInt(4, r.getType().getTypeId());
			pstate.setString(5, r.getDescription());
			
			ResultSet rs = pstate.getGeneratedKeys();
			if(rs.next()) {
				r.setReimbursmentId(rs.getInt(1));
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return r;
	}

	@Override
	public Set<Reimbursement> getReimbursementByUser(User user) {
		String sql = "select * from allReimbursments where a_id = ?;";
		Set<Reimbursement> reimbs = new HashSet<>();
		
		try(Connection conn = cu.getConnection()){
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setInt(1, user.getUserId());
			
			ResultSet rs = pst.executeQuery();
			while(rs.next()) {
				reimbur.add(makeReimbursment(rs));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return reimbur;
	}

	@Override
	public Set<Reimbursment> getAllReimbursments() {
		String sql = "select * from allReimbursments;";
		Set<Reimbursment> reimbs = new HashSet<>();
		
		try(Connection conn = cu.getConnection()){
			PreparedStatement pst = conn.prepareStatement(sql);
			
			ResultSet rs = pst.executeQuery();
			while(rs.next()) {
				reimbur.add(createReimbursement(rs));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return reimbur;
	}

	@Override
	public Set<Reimbursement> getReimbursementsByStatus(ReimbursmentStatus status) {
		String sql = "select * from allReimbursments where status_id = ?;";
		
		Set<Reimbursment> reimbs = new HashSet<>();
		
		
		try(Connection conn = cu.getConnection()){
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setInt(1, status.getStatusId());
			
			ResultSet rs = pst.executeQuery();
			while(rs.next()) {
				reimbs.add(makeReimbursment(rs));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return reimbs;
	}

	@Override
	public boolean updateReimbursmentStatus(Reimbursment r, ReimbursmentStatus status) {
		String sql ="update ers_reimbursement set reimb_status_id = ?, reimb_resolved = current_timestamp, reimb_resolver = ? where reimb_id =?;";
		
		try(Connection conn = cu.getConnection()){
			PreparedStatement pst = conn.prepareStatement(sql);
			
			pst.setInt(1, status.getStatusId());
			pst.setInt(2, r.getResolver().getUserId());
			pst.setInt(3, r.getReimbursmentId());
			
			int rs = pst.executeUpdate();
			if (rs == 1) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	
	public ReimbursmentStatus getStatusByName(String status) {
		String sql = "select * from ers_reimbursment_status where reimb_status = ?";
		ReimbursmentStatus r= new ReimbursmentStatus();;
		
		try(Connection conn = cu.getConnection()){
			
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setString(1, status);
			
			ResultSet rs = pst.executeQuery();
			if(rs.next()) {
				r.setStatus(status);
				r.setStatusId(rs.getInt(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return r;
	}
	
	public ReimbursmentStatus getStatusById(int id) {
		String sql = "select * from ers_reimbursment_status where reimb_status_id = ?";
		ReimbursmentStatus r= new ReimbursmentStatus();;
		
		try(Connection conn = cu.getConnection()){
			
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			
			ResultSet rs = pst.executeQuery();
			if(rs.next()) {
				r.setStatus(rs.getString(2));
				r.setStatusId(id);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return r;
	}
	public ReimbursmentType getTypeByName(String name) {
		String sql = "select * from ers_reimbursment_type where reimb_type = ?";
		ReimbursmentType rt= new ReimbursmentType((int i, String string);;
		
		try(Connection conn = cu.getConnection()){
			
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setString(1, name);
			
			ResultSet rs = pst.executeQuery();
			if(rs.next()) {
				rt.setType(name);
				rt.setTypeId(rs.getInt(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rt;
	}
	public ReimbursmentType getTypeById(int id) {
		String sql = "select * from ers_reimbursment_type where reimb_type_id = ?";
		ReimbursmentType r= new ReimbursmentType((int i, String string);;
		
		try(Connection conn = cu.getConnection()){
			
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			
			ResultSet rs = pst.executeQuery();
			if(rs.next()) {
				r.setTypeId(id);
				r.setType(rs.getString(2));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return r;
	}
	
	public Reimbursment makeReimbursment(ResultSet rs) throws SQLException{
		ReimbursmentStatus status = new ReimbursmentStatus(rs.getInt("status_id"), rs.getString("status_name"));
		ReimbursmentType type = new ReimbursmentType(rs.getInt("type_id"), rs.getString("type_name"));
		User author = new User(rs.getInt("a_id"), rs.getString("a_username"), null, null, null, rs.getString("a_email"), null);
		User resolver = new User(rs.getInt("m_id"), rs.getString("m_username"), null, null, null, rs.getString("m_email"), null);
		Reimbursment r = new Reimbursment(rs.getInt("id"), rs.getDouble("amount"), rs.getDate("submitted"), 
				rs.getDate("resolved"), rs.getString("description"), rs.getString("receipt"), author, resolver, status, type);
		return r;
	}
}