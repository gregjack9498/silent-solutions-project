package com.silent.data;
import com.silent.beans.*;
import java.util.Set;




public interface UserDAO {
	
	public User createUser(User u);
	public Set<User> findUsers();
	public User findUserByUsername(String username);
	public User findUserByEmail(String email);
	public User findUserById(int userId);
	public boolean updateUser(User u);
	public boolean deleteUser(User u);
	
	//creating role,finding role by id, finding role by name
	public Set<Role> findRoles();
	
	public @Override
	Role findRoleById(int id);
	public Role findRoleByName(String name);
	User confirmCredentials(String username, String password);
	public Role getRoleByName(String string);
	
	
	

}
