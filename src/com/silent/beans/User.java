package com.silent.beans;
import java.util.HashSet;
import java.util.Set;

public class User {


	
		private int userId; // primary key
		private String username; // not null, unique
		private String password; // not null
		private String firstName; // not null
		private String lastName; // not null
		private String email; // not null
		private int roleId;
		private Role role;
		private Set<Reimbursment> user;
		
		public User(int i, String string, Object object, Object object2, Object object3, String string2, Object object4) {
			userId = 0;
			username = "";
			password = "";
			firstName = "";
			lastName = "";
			email = "";
			roleId = 0;
			setRole(new Role());
			setUser(new HashSet<Reimbursment>());

		}
		
		public int getUserId() {
			return userId;
		}
		public void setUserId(int userId) {
			this.userId = userId;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}

		public int getRoleId() {
			return roleId;
		}

		public void setRoleId(int roleId) {
			this.roleId = roleId;
		}

		public Role getRole() {
			return role;
		}

		public void setRole(Role role) {
			this.role = role;
		}

		public Set<Reimbursment> getUser() {
			return user;
		}

		public void setUser(Set<Reimbursment> user) {
			this.user = user;
		}


	}
