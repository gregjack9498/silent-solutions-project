package com.silent.beans;

import java.sql.Date;

public class Reimbursment {
	private int reimbursmentId; //primary key
	private double reimbursmentAmount;
	private double submittedAmount ;// object
	private boolean resovled;
	private String description ;
	private String receipt;// object 
	private int author;
	private int resolver;
	private int statusId;
	private int ReimbursmentTypeId;
	
	public Reimbursment() {
		 reimbursmentId = 0;
		reimbursmentAmount =0.0;
		submittedAmount = 0.0;
//		resolved =boolean;
		description ="New";
		receipt = "Object";
		author = 0;
		resolver= 0;
		statusId= 0;
		ReimbursmentTypeId= 0;
		
		
		
		
	}

	public Reimbursment(int int1, double double1, Date date, Date date2, String string, String string2, User author2,
			User resolver2, ReimbursmentStatus status, ReimbursmentType type) {
		// TODO Auto-generated constructor stub
	}

	public int getReimbursmentId() {
		return reimbursmentId;
	}

	public void setReimbursmentId(int reimbursmentId) {
		this.reimbursmentId = reimbursmentId;
	}

	public double getSubmittedAmount() {
		return submittedAmount;
	}

	public void setSubmittedAmount(double submittedAmount) {
		this.submittedAmount = submittedAmount;
	}

	public boolean isResovled() {
		return resovled;
	}

	public void setResovled(boolean resovled) {
		this.resovled = resovled;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReceipt() {
		return receipt;
	}

	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}

	public int getAuthor() {
		return author;
	}

	public void setAuthor(int author) {
		this.author = author;
	}

	public int getResolver() {
		return resolver;
	}

	public void setResolver(int resolver) {
		this.resolver = resolver;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public double getReimbursmentAmount() {
		return reimbursmentAmount;
	}

	public void setReimbursmentAmount(double reimbursmentAmount) {
		this.reimbursmentAmount = reimbursmentAmount;
	}

	public int getReimbursementTypeId() {
		return ReimbursmentTypeId;
	}

	public void setReimbursementTypeId(int reimbursementTypeId) {
		ReimbursmentTypeId = reimbursementTypeId;
	}

	public Reimbursment getStatus() {
		// TODO Auto-generated method stub
		return getStatus();
	}

	public Object getType() {
		// TODO Auto-generated method stub
		return getType();
	}
	
	
}
